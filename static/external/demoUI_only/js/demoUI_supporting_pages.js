//**********************************************
// SUPPORTING DEMO PAGE SCRIPTS

//This script relates to demo supporting pages (workflows, category lists, etc)
//(script is not meant to be used full-view prototype pages)

//script is intended not to mix with underlying prototype components that may be displayed in some supporting pages
//relies on jquery library



//**********************************************
//workflow page setup and functions

//GENERAL WORKFLOW MAP SETUP
demoUI_initiate_workflow_display();


function demoUI_initiate_workflow_display(){

    reorderWorkflowItemsBasedOn_FlexOrder();

    clearExtraneousWorkflowToggleButtons();

    var previousErrorPathsDisplayState = getCookie("displayErrorPaths");
    if (previousErrorPathsDisplayState !== null ) {
        previousErrorPathsDisplayState = eval(previousErrorPathsDisplayState) === true;
        $("#workflow_include_error").prop("checked", previousErrorPathsDisplayState);
        showPathTypes(previousErrorPathsDisplayState, ".path_type_error, .event_class_error", 5);
    }

    var previousOptionalPathsDisplayState = getCookie("displayOptionalPaths");
    if (previousOptionalPathsDisplayState != null ) {
        previousOptionalPathsDisplayState = eval(previousOptionalPathsDisplayState) === true;
        $("#workflow_include_optional").prop("checked", previousOptionalPathsDisplayState);
        showPathTypes(previousOptionalPathsDisplayState, ".path_type_optional, .event_class_optional", 10);
    }

    var previousPageSamplesDisplayState = getCookie("displayPageSamples");
    if (previousPageSamplesDisplayState != null ) {
        previousPageSamplesDisplayState = eval(previousPageSamplesDisplayState) === true;
        $("#workflow_include_samples").prop("checked", previousPageSamplesDisplayState);
        showPageSamples(previousPageSamplesDisplayState);
    }
}


//reorder elements in workflow based on flex order
// (some workflow line styling depends on last-child, which may not work properly if display order from flexbox and and DOM order to not match)
function reorderWorkflowItemsBasedOn_FlexOrder (){

    var $workflowParentItems = $(".workflow_step_destinations, .workflow_steps_events");

    $workflowParentItems.each(function () {
        var $workflowParent = $(this);
        var $pathItems = $workflowParent.children(".workflow_step_destination_wrapper, .workflow_step_event");

        var $newPathItems = $pathItems.sort(function (x, y) {
            return $(x).css('order') > $(y).css('order');
        });

        $workflowParent.append($newPathItems);
    });
}

//remove toggle buttons from destination items with no further events
function clearExtraneousWorkflowToggleButtons (){
    var $all_destinations = $(".workflow_step_destination_wrapper");
    var $destinations_with_events = $all_destinations.has(".workflow_steps_events");
    var $destinations_without_events = $all_destinations.not($destinations_with_events);

    $destinations_without_events.find(".destination_toggle_button").remove();
}


//WORKFLOW - EXPAND/COLLAPSE PATHS FUNCTIONALITY

//add control to workflow destinations to show/hide related action paths and descendants
$(".destination_toggle_button").click(function(){
    workflow_toggle_item($(this),"destination");
});

//add control to workflow events to show/hide related destination paths and descendants
$(".event_toggle_button").click(function(){
    workflow_toggle_item($(this),"event");
});

//add control to workflow events to show all paths
$("#workflow_expand_all").click(function(){
    workflow_displayDetailPaths("all");
});

//add control to workflow events to hide all paths
$("#workflow_collapse_all").click(function(){
    workflow_displayDetailPaths("none");
});

//main function to hide/show all path details
function workflow_displayDetailPaths(state) {

    var showPaths;

    if (state === "all") {
        showPaths = true;
    } else if (state === "none") {
        showPaths = false;
    }

    var $all_event_toggle_buttons = $(".event_toggle_button");
    workflow_toggle_item($all_event_toggle_buttons,"event", showPaths);

    var $all_destination_toggle_buttons = $(".destination_toggle_button");
    workflow_toggle_item($all_destination_toggle_buttons,"destination", showPaths);
}


//main function to hide/show individual event and destination path details
function workflow_toggle_item(toggleControl, itemType, showDetails) {
    var $toggleControl = $(toggleControl);

    var itemDetailsSelector = "";
    if (itemType === "event") {
        itemDetailsSelector = ".workflow_step_destinations";
    } else {
        itemDetailsSelector = ".workflow_steps_events";
    }

    var $detailItems = $toggleControl.parent().siblings(itemDetailsSelector);

    if (showDetails === true) {
        $detailItems.show();
        $toggleControl.removeClass("collapsed")
    } else if (showDetails === false) {
        $detailItems.hide();
        $toggleControl.addClass("collapsed")
    } else {
        $detailItems.toggle();
        $toggleControl.toggleClass("collapsed")
    }
}



//WORKFLOW - INCLUDE/REMOVE PATH TYPES FROM VIEW (error, optional)
$("#workflow_include_error").click( function() {
    var showPaths = $(this).prop("checked");
    showPathTypes(showPaths, ".path_type_error, .event_class_error", 5 )
    setCookie("displayErrorPaths", showPaths);
});

$("#workflow_include_optional").click( function() {
    var showPaths = $(this).prop("checked");
    showPathTypes(showPaths, ".path_type_optional, .event_class_optional", 10 )
    setCookie("displayOptionalPaths", showPaths);
});

function showPathTypes(showPaths, pathsSelector, defaultDisplayOrderForType) {

    var $relatedPaths = $(pathsSelector);

    $relatedPaths.toggle(showPaths);

    //ensure hidden items are pushed to top of order in DOM for proper application of last-child styling
    //(and also put back in place when shown once again)
    var pathTypeDisplayOrderValue;

    if (showPaths === true) {
        pathTypeDisplayOrderValue = defaultDisplayOrderForType;
    } else {
        pathTypeDisplayOrderValue = -100;
    }

    $relatedPaths.css("order", pathTypeDisplayOrderValue);

    reorderWorkflowItemsBasedOn_FlexOrder();
}




//WORKFLOW - ZOOM CONTROL FUNCTIONALITY
var initialScale = 1;
var currentZoomLevel = 1;
var zoomRatio = 1.2;

$("#workflow_zoom_in").click(function () {
    workflow_changeZoomLevel(currentZoomLevel * zoomRatio);
});

$("#workflow_zoom_out").click(function () {
    workflow_changeZoomLevel(currentZoomLevel / zoomRatio);
});

$("#workflow_reset_zoom").click(function () {
    workflow_changeZoomLevel(initialScale);
});

//main zoom control function
function workflow_changeZoomLevel(zoomLevel) {
    $(".workflow_steps").css("transform", "scale(" + zoomLevel + ")");
    currentZoomLevel = zoomLevel;
}




//WORKFLOW - HIDE SHOW PAGE SAMPLES WITHIN WORKFLOW
$("#workflow_include_samples").click( function() {
    var showSamples = $(this).prop("checked");
    showPageSamples(showSamples);
});

function showPageSamples(displayState){
    $(".workflow_page_sample_wrapper").toggle(displayState);
    setCookie("displayPageSamples", displayState);
}




//WORKFLOW - CONTROL FULL-PAGE VIEW
$("#workflow_page_full_view_open").click(function () {
    $(".workflow_control_bar").addClass("full_view");
    $(".workflow_wrapper").addClass("full_view");

    $("#workflow_page_full_view_close").show();
    $("#workflow_page_full_view_open").hide();
});

$("#workflow_page_full_view_close").click(function () {
    $(".workflow_control_bar").removeClass("full_view");
    $(".workflow_wrapper").removeClass("full_view");

    $("#workflow_page_full_view_close").hide();
    $("#workflow_page_full_view_open").show();
});
