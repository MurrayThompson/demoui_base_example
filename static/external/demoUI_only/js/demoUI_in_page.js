//**********************************************
// IN-PROTOTYPE-PAGE METADATA DEMO SCRIPTS

//This script relates to metatdata features (page details, annotations, etc)
//shown within full-view prototype pages
//
//script is intended not to mix with underlying prototype
//relies on jquery library


//**********************************************
//GET MAIN METADATA SECTIONS
var $meta_block_annotations = $('.demoUI_only.annotations_summary');
var $meta_block_page_details = $('.demoUI_only.page_details');
//**********************************************


//**********************************************
//DEMO MENU
//setup top page controls
//**********************************************

if ($(".demoUI_only.annotation_item").length === 0) { //remove toggle button if no annotations exist in the page
    $("#demoUI_only_annotations_summary_toggle_button").remove();
}

$("#demoUI_only_annotations_summary_toggle_button").click(function(){ //annotation summary toggle button set up in page controls
    if (demoUI_check_if_overtop_other_meta_block($meta_block_page_details)) {
        demoUI_flip_meta_blocks_order();
    } else {
        $meta_block_annotations.removeClass("full");
        demoUI_set_page_meta_info_display_state($meta_block_annotations, "toggle");
        demoUI_setup_page_meta_info_display_state_transition($meta_block_annotations);
    }
    return false;
});


$("#demoUI_only_page_details_toggle_button").click(function(){ //page details toggle button in page controls
    if (demoUI_check_if_overtop_other_meta_block($meta_block_annotations)) {
        demoUI_flip_meta_blocks_order();
    } else {
        $meta_block_page_details.removeClass("full");
        demoUI_set_page_meta_info_display_state($meta_block_page_details, "toggle");
        demoUI_setup_page_meta_info_display_state_transition($meta_block_page_details);
    }
    return false;
});


//**********************************************
//GENERAL METABLOCK SETUP AND CONTROLS
//(display state - hide/show/expand/collapse, position control, overlap, etc)
//**********************************************


//**********************************************
//setup page meta blocks and related controls

demoUI_setup_meta_block($meta_block_annotations);
demoUI_setup_meta_block($meta_block_page_details);

function demoUI_setup_meta_block($meta_block) {
    $meta_block.find(".meta_block_close_button").click(function(){
        $meta_block.removeClass("full");
        demoUI_set_page_meta_info_display_state($meta_block, "hide");
        demoUI_setup_page_meta_info_display_state_transition($meta_block);
    });

    $meta_block.find(".meta_block_move_to .left").click(function(){
        demoUI_set_page_meta_info_position($meta_block, "left");
    });

    $meta_block.find(".meta_block_move_to .right").click(function(){
        demoUI_set_page_meta_info_position($meta_block, "right");
    });

    $meta_block.find(".meta_block_move_to .top").click(function(){
        demoUI_set_page_meta_info_position($meta_block, "top");
    });

    $meta_block.find(".meta_block_move_to .bottom").click(function(){
        demoUI_set_page_meta_info_position($meta_block, "bottom");
    });

    $meta_block.find(".meta_block_expand .expand").click(function(){
        $meta_block.addClass("full");
    });
    $meta_block.find(".meta_block_expand .collapse").click(function(){
        $meta_block.removeClass("full");
    });
}


//page meta block functions

function demoUI_get_page_meta_info_display_state(metaBlock){

    if (metaBlock.is(".slide-in-left, .slide-in-right, .slide-in-top, .slide-in-bottom" )) {
        return "show";
    } else {
        return "hide";
    }
}

function demoUI_set_page_meta_info_display_state(metaBlock, show_state){

    var $direction = demoUI_get_page_meta_info_position(metaBlock);
    var $slide_class = "slide-in-" + $direction;

    if (show_state == "toggle") {
        metaBlock.toggleClass($slide_class);
        demoUI_bring_meta_block_to_front(metaBlock);
    }
    if (show_state == "show") {
        metaBlock.addClass($slide_class);
        demoUI_bring_meta_block_to_front(metaBlock);
    }
    if (show_state == "hide") {metaBlock.removeClass($slide_class);}

    if (metaBlock.is("." + $slide_class )) {
        setCookie(metaBlock.selector + "displayState", "show");
    } else {
        setCookie(metaBlock.selector + "displayState", "hide");
    }
}


function demoUI_get_page_meta_info_position(metaBlock){
        if (metaBlock.hasClass("right")) {return "right";}
        if (metaBlock.hasClass("left")) {return "left";}
        if (metaBlock.hasClass("top")) {return "top";}
        if (metaBlock.hasClass("bottom")) {return "bottom";}
}


function demoUI_set_page_meta_info_position(metaBlock, toDirection){

    var $display_state = demoUI_get_page_meta_info_display_state(metaBlock);

    metaBlock.removeClass('left right top bottom full');
    metaBlock.addClass(toDirection);

    if ($display_state == "show"){
        metaBlock.removeClass("slide-in-left slide-in-right slide-in-top slide-in-bottom");
        metaBlock.addClass("slide-in-" + toDirection);
    } else {
        metaBlock.removeClass("slide-in-left slide-in-right slide-in-top slide-in-bottom");
    }

    setCookie(metaBlock.selector + "displayDirection", toDirection);
}


function demoUI_initiate_page_meta_info_block_display(metaBlock){

    var previousDisplayState = getCookie(metaBlock.selector + "displayState");
    if (previousDisplayState ) { demoUI_set_page_meta_info_display_state(metaBlock, previousDisplayState); }

    var previousDirection = getCookie(metaBlock.selector + "displayDirection");
    if (previousDirection ) { demoUI_set_page_meta_info_position(metaBlock, previousDirection); }

    var previousLayerValue = getCookie(metaBlock.selector + "zValue");
    if (previousLayerValue ) { demoUI_set_page_meta_info_layering(metaBlock, previousLayerValue); }

}


function demoUI_set_page_meta_info_layering(metaBlock, zValue) {
    metaBlock.css('z-index', zValue );
    demoUI_bring_meta_block_to_front(); //set page controls above meta info
}


function demoUI_bring_meta_block_to_front(metaBlock) {
    var $page_controls = $(".demoUI_only.page-controls");
    var $controls_z = $page_controls.css('z-index');

    var $meta_block_annotations = $('.demoUI_only.annotations_summary');
    var $annotations_z = $meta_block_annotations.css('z-index');

    var $meta_block_page_details = $('.demoUI_only.page_details');
    var $details_z = $meta_block_page_details.css('z-index');

    var $min_z = Math.min($annotations_z, $details_z, $controls_z);

    $page_controls.css('z-index', $min_z + 2);

    if (metaBlock) {
        $meta_block_annotations.css('z-index', $min_z);
        $meta_block_page_details.css('z-index', $min_z);
        metaBlock.css('z-index', $min_z + 1);

        setCookie($meta_block_annotations.selector + "zValue", $meta_block_annotations.css('z-index'));
        setCookie($meta_block_page_details.selector + "zValue", $meta_block_page_details.css('z-index'));
    }
}

function demoUI_check_if_overtop_other_meta_block($selectedMetablock) {
    var $meta_block_annotations = $('.demoUI_only.annotations_summary');
    var $meta_block_page_details = $('.demoUI_only.page_details');

    var $bothDisplayed = demoUI_get_page_meta_info_display_state($meta_block_annotations) == "show" && demoUI_get_page_meta_info_display_state($meta_block_page_details) == "show";
    var $samePosition = demoUI_get_page_meta_info_position($meta_block_annotations) == demoUI_get_page_meta_info_position($meta_block_page_details);
    var $eitherIsExpandedToFullView = $meta_block_annotations.hasClass("full") || $meta_block_page_details.hasClass("full");

    var $selectedIsOnTop = $selectedMetablock.css("z-index") ==  Math.max($meta_block_annotations.css('z-index'), $meta_block_page_details.css('z-index'));

    if ($bothDisplayed && ( $samePosition || $eitherIsExpandedToFullView )  && $selectedIsOnTop) {
        return true;
    } else {
        return false;
    }
}

function demoUI_flip_meta_blocks_order() {
    var $meta_block_annotations = $('.demoUI_only.annotations_summary');
    var $meta_block_page_details = $('.demoUI_only.page_details');

    var $original_z = $meta_block_annotations.css('z-index');
    $meta_block_annotations.css('z-index', $meta_block_page_details.css('z-index'));
    $meta_block_page_details.css('z-index', $original_z);

    setCookie($meta_block_annotations.selector + "zValue", $meta_block_annotations.css('z-index'));
    setCookie($meta_block_page_details.selector + "zValue",  $meta_block_page_details.css('z-index'));
}


function demoUI_setup_page_meta_info_display_state_transition(metaBlock){
    metaBlock.css("transition-timing-function", "ease-out");
    metaBlock.css("transition-duration", "1s");
}




//**********************************************
//PAGE DETAILS-SPECIFIC SETUP AND CONTROL
//**********************************************

//set up page details meta block at page level
demoUI_initiate_page_meta_info_block_display($meta_block_page_details);

$meta_block_page_details.click(function(e){
    if (demoUI_check_if_overtop_other_meta_block($meta_block_page_details)) {
        if($(e.target).is(".page_details, " +
            ".meta_block_summary_controls, .meta_block_title, .meta_block_move_to, .meta_block_items, " +
            ".description_block, .page_meta_info, .meta-info-block")) {
            demoUI_flip_meta_blocks_order();
        }
    } else {
        demoUI_bring_meta_block_to_front($(this));
    }
});




//**********************************************
//ANNOTATIONS-SPECIFIC SETUP AND CONTROL
//**********************************************

//**********************************************
//set up annotations at page level

if ($(".demoUI_only.annotation_item").length > 0) {
    demoUI_setupAnnotationNumbers();
    demoUI_initiate_page_meta_info_block_display($meta_block_annotations);

    $(window).resize(function () {
        demoUI_resetAnnotationItem_LocalCopies();
    });
}

$meta_block_annotations.click(function(e){
    if (demoUI_check_if_overtop_other_meta_block($meta_block_annotations)) {
        if($(e.target).is(".annotations_summary, " +
            ".meta_block_summary_controls, .meta_block_title, .meta_block_move_to, .meta_block_items")) {
            demoUI_flip_meta_blocks_order();
        }
    } else {
        demoUI_bring_meta_block_to_front($(this));
    }
});


//**********************************************
//individual annotation item functions

$(".demoUI_only.annotation_item").click(function () {
    var $item = $(this);

    if ($item.hasClass("local")) {

    } else {

        if ($item.hasClass("displayed")) {
            demoUI_hideAnnotationItem_LocalCopies($item);
        } else {
            demoUI_showAnnotationItem_LocalCopies($item);
        }
    }

});


function demoUI_setupAnnotationNumbers(){
    //automatically numbers annotations found in page (assumes all initially-available items are unique from each other)
    var i=1;

    $(".demoUI_only.annotation_item").each(function () {
        var $item = $(this);
        var $itemNumber = $($item.find(".annotation_item_number")[0]);
        $itemNumber.html(i);
        i=i+1;
    });
}

function demoUI_addLocalAnnotationClick(reference) {
    //adds ability for locally-displayed annotation indicator to hide/show content in-place after being added to DOM
    reference.click(function () {
        var $item = reference.closest(".annotation_item");

        var $openClass = "open";
        if ($item.hasClass($openClass)) {
            $item.removeClass($openClass);
        } else {
            $item.addClass($openClass);
        }
    });
}


function demoUI_showAnnotationItem_LocalCopies($item){
    $item.addClass("displayed");
    var $selector = $item.attr("selector");

    if ($selector) {
        var $target_objects_all = $($selector);
        var $target_objects_in_annotations_summary = $('.demoUI_only.annotation_item').find($selector);
        var $target_objects_in_page = $target_objects_all.not($target_objects_in_annotations_summary);

        $target_objects_in_page.each(function () {
            var $target_element = $(this);

            if ($target_element.is(":visible")) {
                var $target_element_offset = $target_element.offset();
                var $itemCopy = $item.clone();

                $("body").append($itemCopy);

                demoUI_setAnnotationItem_LocalCopies_Classes($itemCopy);
                demoUI_setAnnotationItem_LocalCopies_Position($itemCopy,$target_element_offset);

                var $number = $itemCopy.find(".annotation_item_number");
                demoUI_addLocalAnnotationClick($number);

                if ($itemCopy.attr('show_element_overlay') === 'true') {
                    demoUI_addElementAnnotationElementOverlay($itemCopy, $target_element);
                }
            } else {
                $item.addClass("hidden_elements");
            }
        });
    }
}

function demoUI_setAnnotationItem_LocalCopies_Classes ($localCopy){
    $localCopy.removeClass("displayed");
    $localCopy.removeClass("hidden_elements");
    $localCopy.addClass("local");
}

function demoUI_setAnnotationItem_LocalCopies_Position ($localCopy, $offset){
    $localCopy.css("position", "absolute");

    $offset = demoUI_checkAnnotationItem_LocalCopies_Position ($localCopy, $offset);

    $localCopy.css("left", $offset.left);
    $localCopy.css("top", $offset.top);
}

function demoUI_checkAnnotationItem_LocalCopies_Position ($localCopy, $offset) {
    var $otherLocalCopies = $(".annotation_item.local");
    $otherLocalCopies.each( function (){
        var $current = $(this);
        if ($current.offset().top === $offset.top && $current.offset().left === $offset.left ) {
            $offset.top = $offset.top - 7;
            $offset.left = $offset.left + 21;
            $offset = demoUI_checkAnnotationItem_LocalCopies_Position ($localCopy, $offset);
        }
    });

    return $offset;
}

function demoUI_hideAnnotationItem_LocalCopies($item){
    $item.removeClass("displayed");
    $item.removeClass("hidden_elements");

    var $itemNumber = $item.find(".annotation_item_number")[0].innerHTML;

    $(".annotation_item.local").each(function () {
        var $currentItem = $(this);
        if ($currentItem.find(".annotation_item_number")[0].innerHTML=== $itemNumber) {
            $currentItem.remove();
        }
    });
}

function demoUI_resetAnnotationItem_LocalCopies (){
    //for resetting locally-placed indicators after page resize or other actions with the page that may hav changed element positions
    var $displayed_annotation_items = $(".demoUI_only.annotation_item.displayed");

    $displayed_annotation_items.each(function () {
        var $this = $(this);
        demoUI_hideAnnotationItem_LocalCopies($this);
        demoUI_showAnnotationItem_LocalCopies($this);
    });
}

function demoUI_addElementAnnotationElementOverlay ($item, $element) {
    //places an optional overlay to highlight element along with numerical indicator relatad to annotation
    var styleString= "top: 0; " + "left: 0; " + "width: " + $element.outerWidth() + "px; " + "height: " + $element.outerHeight() + "px;";
    var $highlightBoxHTML = "<div class='demoUI_only annotation_element_overlay' style='" + styleString + "'></div>";

    $item.append($highlightBoxHTML);
}

