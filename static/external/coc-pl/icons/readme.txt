There are two font icon libraries here:
- cicon
- font-awesome

The cicon library consists of custom-made City of Calgary icons.

The font-awesome library consists of icons from https://fontawesome.com/ with custom adjustments to the positions of the icons, made by the Pattern Library team. No further changes have been made.

Import the libraries as required for your project.