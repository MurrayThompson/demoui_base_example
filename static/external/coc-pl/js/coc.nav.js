var COC = COC || {};
COC.Nav = {

	Init: function() {
		COC.Nav.SetupEvents();
		COC.Nav.SetupPageResizeEvent();
	},
	InitTopicNav: function() {
		COC.Nav.CreateTopicNav();
		COC.Nav.SetupTopicNavEvents();
	},
	InitLocalNav: function() {
		//currently piggybacking on topic nav implementation
		COC.Nav.CreateLocalNav();
		COC.Nav.SetupLocalNavEvents();
	},
	InitMobileBottomNav: function() {
		//currently piggybacking on topic nav implementation
		//COC.Nav.CreateMobileBottomNav();
		COC.Nav.SetupMobileBottomNavEvents();
	},
	SetupEvents: function() {
		COC.Nav.SetupNavOverlayClickEvent();
		COC.Nav.SetupNavEscEvent('#navId');
		COC.Nav.SetupNavOpenCloseClickEvents();
		COC.Nav.SetupSubNavOpenCloseEvents('#navId');
		COC.Nav.PreventCloseIfClickedInsideSubNav('#navId');
		COC.Nav.SetupNavItemHoverEvents('#navId');
	},
	SetupTopicNavEvents: function() {
		COC.Nav.SetupNavEscEvent('#topicMenu');
		COC.Nav.SetupTopicNavOpenCloseClickEvents();
		COC.Nav.SetupSubNavOpenCloseEvents('#topicMenu');
		COC.Nav.PreventCloseIfClickedInsideSubNav('#topicMenu');
	},
	//NOTE: Switched #localNav to .localNav here and in other parts of code to support multiple local navs - see Election 2017 single page web app as an example of use case for multiple local navs
	SetupLocalNavEvents: function() {
		//currently piggybacking on topic nav implementation
		COC.Nav.SetupNavEscEvent('.localNav');
		COC.Nav.SetupLocalNavOpenCloseClickEvents();
		COC.Nav.SetupLocalNavLinkCloseEvents();
		//COC.Nav.SetupSubNavOpenCloseEvents('.localNav');
		//COC.Nav.PreventCloseIfClickedInsideSubNav('.localNav');
	},
	SetupMobileBottomNavEvents: function() {
		//currently piggybacking on topic nav implementation
		COC.Nav.SetupNavEscEvent('.mobileBottomNav');
		COC.Nav.SetupMobileBottomNavOpenCloseClickEvents();
		COC.Nav.SetupMobileBottomNavLinkCloseEvents();
		//COC.Nav.SetupSubNavOpenCloseEvents('.mobileBottomNav');
		//COC.Nav.PreventCloseIfClickedInsideSubNav('.mobileBottomNav');
	},
	OpenNav: function() {
		COC.Nav.OpenNavLogic();
		COC.Nav.ToggleDarkPageOverlay();
	},
	CloseNav: function() {
		COC.Nav.CloseNavLogic();
		COC.Nav.CloseSubNavs();
		COC.Nav.ToggleDarkPageOverlay();
	},
	OpenTopicNav: function() {
		COC.Nav.OpenTopicNavLogic();
		COC.Nav.ToggleDarkPageOverlay();
	},
	CloseTopicNav: function() {
		COC.Nav.CloseTopicNavLogic();
		COC.Nav.CloseSubNavs();
		COC.Nav.ToggleDarkPageOverlay();
	},
	OpenLocalNav: function($nav) {
		COC.Nav.OpenLocalNavLogic($nav);
		COC.Nav.ToggleDarkPageOverlay();
	},
	CloseLocalNav: function() {
		COC.Nav.CloseLocalNavLogic();
		COC.Nav.CloseSubNavs();
		COC.Nav.ToggleDarkPageOverlay();
	},
	OpenMobileBottomNav: function($nav) {
		COC.Nav.OpenMobileBottomNavLogic($nav);
		COC.Nav.ToggleDarkPageOverlay();
	},
	CloseMobileBottomNav: function() {
		COC.Nav.CloseMobileBottomNavLogic();
		COC.Nav.CloseSubNavs();
		COC.Nav.ToggleDarkPageOverlay();
	},
	OpenSubNav: function($subNav) {
		COC.Nav.OpenSubNavLogic($subNav);
		COC.Nav.ToggleDarkPageOverlay();
	},
	CloseSubNavs: function() {
		COC.Nav.CloseSubNavsLogic();
		COC.Nav.ToggleDarkPageOverlay();
	},
	CloseNavFromOverlay: function() {
		COC.Nav.CloseExpandedMobileNav();
		COC.Nav.CloseSubNavs();
		COC.Nav.ToggleDarkPageOverlay();
	},

	// MAIN FUNCTIONALITY:

	// Open or close global navigation from collapsed state
	CloseNavLogic: function() {
		var $nav = $('#navId');
		$nav.find("#exploreBtn").removeClass("active").show();
		$nav.find("#closeBtn").addClass("active").hide();
		$nav.removeClass('active');
		$nav.find('.nav-menu').css('z-index', '15').removeClass('expanded').addClass('collapsed');
		$("#cocgl-mainHeaderContainer, #coc_re-mainNav, nav.pushy, #olsh-nav, #cochr-wellness-mainHeaderContainer").css('z-index', '9999');
	},
	OpenNavLogic: function() {
		var $nav = $('#navId');
		$nav.find("#exploreBtn").addClass("active").hide();
		$nav.find("#closeBtn").removeClass("active").show();
		$nav.addClass('active');
		$nav.find('.nav-menu').css('z-index', '3000').removeClass('collapsed').addClass('expanded');
		$("#cocgl-mainHeaderContainer, #coc_re-mainNav, nav.pushy, #olsh-nav, #cochr-wellness-mainHeaderContainer").css('z-index', '9998');
	},

	// Open or close topic navigation from collapsed state - see Ward subsites for secondary topic nav
	CloseTopicNavLogic: function() {
		var $nav = $('#topicMenu');
		if($nav.length === 0)
			return;
		$nav.find(".expandBtn").removeClass("active").show();
		$nav.find(".collapseBtn").addClass("active").hide();
		$nav.removeClass('active');
		$nav.find('.nav-menu').removeClass('expanded').addClass('collapsed');
	},
	OpenTopicNavLogic: function() {
		var $nav = $('#topicMenu');
		$nav.find(".expandBtn").addClass("active").hide();
		$nav.find(".collapseBtn").removeClass("active").show();
		$nav.addClass('active');
		$nav.find('.nav-menu').css('z-index', '3000').removeClass('collapsed').addClass('expanded');
	},
	EnsureNavOrTopicNavOpen: function($subNav) {
		if(COC.Nav.CheckIfGlobalSubNav($subNav))
			COC.Nav.OpenNavLogic();
		else
			COC.Nav.OpenTopicNavLogic();
	},

	// Open or close local navigation from collapsed state - see Election subsite for secondary local nav
	CloseLocalNavLogic: function() {
		var $nav = $('.localNav');
		if($nav.length === 0)
			return;
		$nav.find(".expandBtn").removeClass("active").show();
		$nav.find(".collapseBtn").addClass("active").hide();
		$nav.removeClass('active');
		$nav.find('.nav-menu').removeClass('expanded').addClass('collapsed');
	},
	OpenLocalNavLogic: function($nav) {
		//var $nav = $('.localNav');
		$nav.find(".expandBtn").addClass("active").hide();
		$nav.find(".collapseBtn").removeClass("active").show();
		$nav.addClass('active');
		$nav.find('.nav-menu').css('z-index', '3000').removeClass('collapsed').addClass('expanded');
	},

	// Open or close MobileBottom navigation from collapsed state - see Election WEB APP for secondary MobileBottom nav
	CloseMobileBottomNavLogic: function() {
		var $nav = $('.mobileBottomNav');
		if($nav.length === 0)
			return;
		$nav.find(".expandBtn").removeClass("active").show();
		$nav.find(".collapseBtn").addClass("active").hide();
		$nav.removeClass('active');
		$nav.find('.action-panel').removeClass('expanded').addClass('collapsed');
	},
	OpenMobileBottomNavLogic: function($nav) {
		//var $nav = $('.mobileBottomNav');
		$nav.find(".expandBtn").addClass("active").hide();
		$nav.find(".collapseBtn").removeClass("active").show();
		$nav.addClass('active');
		$nav.find('.action-panel').css('z-index', '3000').removeClass('collapsed').addClass('expanded');
	},
	/*
	EnsureNavOrTopicOrLocalNavOpen: function($subNav) {
		//TODO:Add logic here for local nav
		if(COC.Nav.CheckIfGlobalSubNav($subNav))
			COC.Nav.OpenNavLogic();
		else
			COC.Nav.OpenTopicNavLogic();
	},*/

	// Open or close sub-navigation items wtihin the global or topic navigation
	ToggleSubNavClosedOrOpenState: function($subNav) {
		if($subNav.is(':visible')) {
			COC.Nav.CloseSubNavs();
		} else {
			//ensure mega nav closes when user tabs clicks or focus on another expandable nav item in menu then, open sub nav
			COC.Nav.CloseSubNavs();
			COC.Nav.OpenSubNav($subNav);
		}
	},
	CloseSubNavsLogic: function() {
		var $subNavs = $('.nav-menu .sub-nav');
		if($subNavs.length === 0)
			return;
		$subNavs.each(function() {
			//close each sub-nav that is open - even if they aren't visible to support toggling between mobile and desktop versions of menu
			COC.Nav.CloseSubNavLogic($(this));
		});
	},
	CloseSubNavLogic: function($subNav) {
		var $subNavParent = $subNav.closest('.nav-item');
		var $subNavLink = $subNavParent.find('.coc-secondary-9L');
		//manage aria
		$subNav.attr('aria-expanded','false');
		$subNavLink.attr('aria-expanded','false');
		//manage role - mobile version does not use role="dialog"
		$subNav.removeAttr('role');
		//manage classes
		$subNav.removeClass('open');
		$subNavParent.removeClass('open');
		var focusLink = $subNavLink.hasClass('open');
		$subNavLink.removeClass('open');		
		//manage additional events
		COC.Nav.RemoveSubNavCloseEvents($subNav);
		COC.Nav.RemoveSubNavDialogFocusLoopEvent();		
		$subNav.hide();
		//Note: no longer setting focus back to link if related subnav just closed
		//Note: have since gone back on last note are are setting focus back to link if subnav just closed..
		if(focusLink) $subNavLink.focus();
	},
	OpenSubNavLogic: function($subNav) {
		COC.Nav.EnsureNavOrTopicNavOpen($subNav);
		var $subNavParent = $subNav.closest('.nav-item');
		var $subNavLink = $subNavParent.find('.coc-secondary-9L');
		//manage aria
		$subNav.attr('aria-expanded','true');
		$subNavLink.attr('aria-expanded','true');
		//manage role - mobile version does not use role="dialog"
		if(!COC.Nav.CheckIfMobileNav() && COC.Nav.CheckIfGlobalSubNav($subNav))
			$subNav.attr('role','dialog');
		//manage classes
		$subNav.addClass('open');
		$subNavParent.addClass('open');
		$subNavLink.addClass('open');
		//when an element with role="dialog" becomes visible this prompts screenreaders
		$subNav.show();
		//manage additional events
		COC.Nav.SetupSubNavCloseEvents($subNav);
		COC.Nav.SetupSubNavDialogFocusLoopEventIfDesktop();
		//Note: no need to set focus - when element with role="dialog" shows it prompts the screenreader to act?
		//$subNav.find('.closeSubNavGroup:first').focus();
		//Note: had trouble with assumption made in previous note - made a wrapper div tabbable and set focus to it
		$subNav.focus();
	},

	// Handle dark page overlay that can appear behind global or topic nav
	HideDarkPageOverlay: function() {
		$('#cocis-nav-active-page-overlay').hide();
	},
	HideDarkPageOverlayIfDesktop: function() {
		if(!COC.Nav.CheckIfMobileNav())
			COC.Nav.HideDarkPageOverlay();
	},
	HideDarkPageOverlayIfDesktopTopicNav: function() {
		if(!COC.Nav.CheckIfMobileNav() && $('#topicMenu').length > 0)
			COC.Nav.HideDarkPageOverlay();
	},
	ShowDarkPageOverlay: function() {
		$('#cocis-nav-active-page-overlay').show();
	},
	ShowDarkPageOverlayIfMobile: function() {
		if(COC.Nav.CheckIfMobileNav())
			COC.Nav.ShowDarkPageOverlay();
	},
	ShowDarkPageOverlayIfDesktopGlobalNav: function($subNav) {
		if(!COC.Nav.CheckIfMobileNav() && $subNav.closest('#navId').length > 0)
			COC.Nav.ShowDarkPageOverlay();
	},
	ToggleDarkPageOverlay: function() {
		if(COC.Nav.CheckIfMobileNav()) {
			if(COC.Nav.CheckIfNavOpen()) {
				COC.Nav.ShowDarkPageOverlay();
			} else {
				COC.Nav.HideDarkPageOverlay();
			}
		} else {
			if($('#navId .nav-menu.expanded .sub-nav.open').length > 0) {
				COC.Nav.ShowDarkPageOverlay();
			} else {
				COC.Nav.HideDarkPageOverlay();
			}
		}
	},
	CheckIfMobileNav: function() {
		return($('#closeBtn:visible, #exploreBtn:visible, .collapseBtn:visible, .expandBtn:visible').length > 0);
	},
	CheckIfGlobalSubNav: function($subNav) {
		return($subNav.find('.closeSubNavGroup:first').length > 0);
	},
	CheckIfNavOpen: function() {
		return($('.nav-menu.expanded').length > 0);
	},
	CloseExpandedMobileNav: function() {
		if(COC.Nav.CheckIfMobileNav()) {
			COC.Nav.CloseNav();
			COC.Nav.CloseTopicNav();
			COC.Nav.CloseLocalNav();
		}
	},
	CloseNavIfDesktop: function() {
		if(!COC.Nav.CheckIfMobileNav()) {
			COC.Nav.CloseNav();
			COC.Nav.CloseTopicNav();
			COC.Nav.CloseLocalNav();
		}
	},

	//EVENT MANAGEMENT:
	
	SetupSubNavOpenCloseEvents: function(containerSel) {
		$(containerSel + ' .nav-menu .coc-secondary-9L').each(function() {
			// handles opening sub-nav item on click and also closing on click
			$(this).on('click', function(e) {
				var $subNav = $(this).next('.sub-nav');
				if($subNav.length > 0) {
					COC.Nav.ToggleSubNavClosedOrOpenState($subNav);
					//prevent empty hashtag from appearing in URL
					e.preventDefault();
				}
			});
		});
	},
	PreventCloseIfClickedInsideSubNav: function(containerSel) {
		$(containerSel + ' .nav-menu .sub-nav').each(function() {
			// prevents close if clicked inside
			$(this).on('click', function(e){
				e.stopPropagation();
			});
		});
	},
	SetupNavItemHoverEvents: function(containerSel) {
		$(containerSel + ' li').on('mouseover', function(currentHoverInNestedList) {
			currentHoverInNestedList.stopPropagation(); $(this).addClass('currentHover');
		});
		$(containerSel + ' li').on('mouseout', function() {
			$(this).removeClass('currentHover');
		});
	},
	SetupNavOpenCloseClickEvents: function() {
		$('#exploreBtn').on('click', function() {
			COC.Nav.OpenNav();
		});
		$('#closeBtn').on('click', function() {
			COC.Nav.CloseNav();
		});
	},
	SetupTopicNavOpenCloseClickEvents: function() {
		$('#topicMenu .expandBtn').on('click', function() {
			COC.Nav.OpenTopicNav();
		});
		$('#topicMenu .collapseBtn').on('click', function() {
			COC.Nav.CloseTopicNav();
		});
	},
	SetupLocalNavOpenCloseClickEvents: function() {
		$('.localNav .expandBtn').on('click', function() {
			var $nav = $(this).parents('.localNav');
			COC.Nav.OpenLocalNav($nav);
		});
		$('.localNav .collapseBtn').on('click', function() {
			COC.Nav.CloseLocalNav();
		});
	},
	SetupLocalNavLinkCloseEvents: function() {
		var $nav = $('.localNav');
		$nav.find('.nav-item a').on('click', function() {
			COC.Nav.CloseLocalNav();
		});
	},
	SetupMobileBottomNavOpenCloseClickEvents: function() {
		$('.mobileBottomNav .expandBtn').on('click', function() {
			var $nav = $(this).closest('.mobileBottomNav');
			COC.Nav.OpenMobileBottomNav($nav);
		});
		$('.mobileBottomNav .collapseBtn').on('click', function() {
			COC.Nav.CloseMobileBottomNav();
		});
		//Accessibility: On enter keypress trigger click - requires both .action-button elements have tabindex="0"
		$('.mobileBottomNav .action-button').on('keydown', function(e) {
			if (e.keyCode === 13)
				$(this).click();
		});
	},
	SetupMobileBottomNavLinkCloseEvents: function() {
		var $nav = $('.mobileBottomNav');
		$nav.find('.action-items .action').on('click', function() {
			COC.Nav.CloseMobileBottomNav();
		});
	}, 
	SetupNavOverlayClickEvent: function() {
		$('#cocis-nav-active-page-overlay').on('click', function() {
			COC.Nav.CloseNavFromOverlay();
		});
	},
	SetupNavEscEvent: function(containerSel) {
		$(containerSel + ' .nav-menu').on('keydown', function(e) {
			if (e.keyCode === 27)
				COC.Nav.CloseNavFromOverlay();
		});
	},
	SetupSubNavCloseEvents: function($subNav) {
		$subNav.find('.closeSubNavGroup').each(function() {
			$(this).on('click', function() {
				COC.Nav.CloseSubNavs();
			});
		});
	},
	RemoveSubNavCloseEvents: function($subNav) {
		$subNav.find('.closeSubNavGroup').each(function() {
			$(this).off('click');
		});
	},
	SetupSubNavDialogFocusLoopEventIfDesktop: function() {
		//only loop focus within desktop mega nav dialog
		if(COC.Nav.CheckIfMobileNav())
			return;
		//depends on 1 visible sub nav that has role="dialog" and a close button
		if($('.sub-nav.open[role="dialog"]:visible').length === 0)
			return;
		$(document).on('focusin', function(e) {
			var $subNav = $('.sub-nav.open[role="dialog"]:visible').first();
			if($subNav.length === 1 && !$subNav[0].contains(e.target)) {
				// loop focus back to start of dialog tabbing order by focusing on close button
				e.stopPropagation();
				$subNav.find('.closeSubNavGroup:first').focus();
			}
		});
	},
	RemoveSubNavDialogFocusLoopEvent: function() {
		$(document).off('focusin');
	},
	SetupPageResizeEvent: function() {
		$(window).on('resize orientationchange', function () {
			COC.Nav.ToggleDarkPageOverlay();
		});
	},
	RemovePageResizeEvent: function() {
		$(window).off('resize orientationchange');
	},

	// Currently this will only generate and setup a local nav if you are on a site that contains an element with id: "#localNavHtml" (e.g: Elections 2017 > "Information for Voters" page has an element with id: #localNavHtml that is loaded in to the left sidebar through a content editor web part). Alternatively, we could scan the page content area for "h" elements and populate this local nav programatically - there should also be a way to turn this on or off and the same could work for topic nav.
	CreateLocalNav: function() {
		var $localNavHtml = $('#localNavHtml');
		if($localNavHtml.length === 0)
			return;
		COC.Nav.GenerateLocalNavContent();
		COC.Nav.CompleteLocalNavSetup();
		COC.Nav.CheckActiveLocalNavItem();
		//COC.Nav.ResolveDynamicNavBugs();  //TODO: remove if no longer needed
	},
	// Currently we get local nav content that is brought in to a web part usually in the left sidebar and move it in to a local nav container
	GenerateLocalNavContent: function() {
		var localNavHtml = $('#localNavHtml').html();
		$('#localNav .nav-menu').append(localNavHtml);

		// replace title of local nav for the mobile dropdown menu
		var localNavMobileTitle = $('#localNavMobileTitle').html();
		if(localNavMobileTitle) {
			$('#localNav #closeBtn_local').html(localNavMobileTitle);
			$('#localNav #exploreBtn_local').html(localNavMobileTitle);
		}
	},
	// Piggy-backs on some functionality for topic nav that sets the styling for left sidebar and also allows for fixed scrollbar contents on scroll
	CompleteLocalNavSetup: function() {
		$("#cocis-left-column").addClass("topicNavContainer");
		$("#cocis-main").addClass("hasTopicMenu");  // classname used for styling breadcrumb on pages that have local nav
		$('#localNavContainer').show();
		
		if ($("#localNav .expandBtn").is(":visible")) {
			$("#localNav .collapseBtn").click();
		}
	},
	// Checks if the current page url is equal to any items in nav to then set active state
	CheckActiveLocalNavItem: function() {
		$('.localNav li').each(function() {
			var strHref = $(this).find('a:first').attr('href').trim().toLowerCase();
			if(window.location.href.toLowerCase().search(strHref) > -1) {
				$(this).addClass('active');
			}
		});
	},

	// Currently this will only generate and setup a topic nav if you are on a Ward site
	CreateTopicNav: function() {
		var loc = window.location.href.toLowerCase();
		if (loc.indexOf("/citycouncil/ward-") == -1)
			return;
		COC.Nav.GenerateTopicNavContent();
		COC.Nav.CompleteTopicNavSetup();
		COC.Nav.ResolveDynamicNavBugs();  //TODO: remove if no longer needed
	},
	GenerateTopicNavContent: function() {
		// add content to the topic nav placeholder
		var $topicNavTemplate = $('#topicMenuContainer');

		if ($(".menu-vertical .root").length > 0) {
			var siteName = $(".menu-vertical .root > li > a span.menu-item-text:first").text();
			$topicNavTemplate.find("#topicMenu .nav-menu > button").each(function (idx, itm) { $(itm).html($(itm).html().replace("Menu", siteName + " Menu")); });
			$topicNavTemplate.find("#topicMenu .nav-menu").append('<li class="nav-item"><a href="' + $(".menu-vertical .root > li > a").attr("href") + '" tabindex="0">' + siteName + '</a></li>');
			// generate topic nav from out of box <SharePoint:AspMenu>
			$(".menu-vertical .root > li > ul > li").each(function (idx, itm) {
				var menuItem = $('<li class="nav-item"><a href="' + $(itm).find("a.menu-item:first").attr("href") + '" tabindex="0"><span class="menuTitle">' + $(itm).find("a.menu-item:first .menu-item-text").text() + '</span></a></li>');
				if ($(itm).find("ul li").length > 0) {
					menuItem.find("a").attr("href", "#").addClass("coc-secondary-9L");
					menuItem.find("a").after('<div class="sub-nav"><ul></ul></div>');
					$(itm).find("ul li").each(function (idx2, itm2) {
						menuItem.find(".sub-nav ul").append('<li class="nav-item"><a href="' + $(itm2).find("a.menu-item:first").attr("href") + '" tabindex="0"><span class="menuTitle">' + $(itm2).find("a.menu-item:first .menu-item-text").text() + '</span></a></li>');
					});
				}
				$topicNavTemplate.find("#topicMenu .nav-menu").append(menuItem);
			});
		}
		if ($("#ctl00_CurrentNav2").length > 0) {
			var siteName = $("#ctl00_CurrentNav2 > span:first > a").text();
			$topicNavTemplate.find("#topicMenu .nav-menu > button").each(function (idx, itm) { $(itm).html($(itm).html().replace("Menu", siteName + " Menu")); });
			$topicNavTemplate.find("#topicMenu .nav-menu").append('<li class="nav-item"><a href="' + $("#ctl00_CurrentNav2 > span:first > a").attr("href") + '" tabindex="0">' + siteName + '</a></li>');
			$("#ctl00_CurrentNav2 > span").each(function (idx, itm) {
				if ($(itm).find("img").css("width") == '10px') {
					var menuItem = $('<li class="nav-item"><a href="' + $(itm).find("a:first").attr("href") + '" tabindex="0"><span class="menuTitle">' + $(itm).text() + '</span></a></li>');
					$topicNavTemplate.find("#topicMenu .nav-menu").append(menuItem);
				} else { // itm is a sub-item, should be added to parent
					var parentItem = $topicNavTemplate.find("#topicMenu .nav-menu > .nav-item:last");
					if (!parentItem.find("a").hasClass('coc-secondary-9L')) {
						parentItem.find("a").addClass("coc-secondary-9L");
						parentItem.find("a").after('<div class="sub-nav"><ul></ul></div>');
					}
					parentItem.find(".sub-nav ul").append('<li class="nav-item"><a href="' + $(itm).find("a:first").attr("href") + '" tabindex="0"><span class="menuTitle">' + $(itm).find("a:first").text() + '</span></a></li>');
				}
			});
		}
	},
	// Additional workarounds are applied
	CompleteTopicNavSetup: function() {
		$("#cocis-left-column").addClass("topicNavContainer");
		$("#cocis-main").addClass("hasTopicMenu");  // classname used for styling breadcrumb on pages that have topic nav
		$("#topicNavHeader .topic-nav-header").html($(".menu-vertical .root > li > a span.menu-item-text:first").text());
		$('#topicMenuContainer').show();
		
		if ($("#topicMenu .expandBtn").is(":visible")) {
			$("#topicMenu .collapseBtn").click();
		}
	},
	ResolveDynamicNavBugs: function() {
		// This resolves bug that sometimes prevent nav menu items that do not have a sub nav from appearing - resolve by adding an empty sub nav element to them (has to be done after nav menu has been initialized by plugin code)
		//$('#navId > .nav-menu > .nav-item').each(function () { if ($(this).find('div.sub-nav').length === 0) $(this).find('a.sub-nav').removeClass('sub-nav'); });
		$('#topicMenu > .nav-menu > .nav-item').each(function () { if ($(this).find('div.sub-nav').length === 0) $(this).find('a.sub-nav').removeClass('sub-nav'); });
		
		//fix for desktop Safari issue (CNU2015-602) - won't trigger focusin unless <a> has a tabindex
		//Added in to code up above... $("#topicMenu .nav-item > a").attr("tabindex","0");
	},
};