
$('.app-card').click(function() {

    var $this = $(this);

    //get related page modal

    var $modal = $('.cui.modal-window');


    //set modal info to related service values

    $modal.find('.modal-window-header h2').text($this.find('.card-title').text());
    $modal.find('.modal-window-body').text($this.find('.card-body-top p:first-of-type').text());

});



$(".utility-nav-bar .close-button").click(function () {
    $(".utility-nav-bar").removeClass('open');
    $(".utility-nav-bar").addClass('closed');
    $("body").css("overflow","auto");
});

$(".utility-nav-bar .button-settings").click(function () {
    $(".utility-nav-bar").removeClass('closed');
    $(".utility-nav-bar").addClass('open');
    if ( $(".utility-nav-bar").css("box-shadow") == "none" ) {
        $("body").css("overflow","hidden");
    }

});

$(".display-widget.foldable h2").click(function () {
    var $this = $(this);
    var $widget=$this.closest(".display-widget");
    $widget.toggleClass('open');
    $widget.toggleClass('closed');
});
