---
title: "[System step example]"
id: "system_step_example"

type: "system"
layout: "step"

tags: [ "tag 2"]

workflows:
-   name: "example_process_workflow"
    onEvent: 
    -   event: "some success"
        eventType: "system"
        eventClass: "primary"
        goto: 
        -   destination: "system_step_example_2"
            pathType: ["primary"]

        
---