---
title: "[Page Example 3]"
id: "page_example_3"

tags: [ "tag 1", "tag 2"]
status: ["beta"]


workflows:
-   name: "example_workflow"
    onEvent: 
    -   event: "back to start"
        eventType: "user action"
        eventClass: "optional"
        goto: 
        -   destination: "page_example"
            pathType: ["return"]
    -   event: "start a user-initiated system thing"
        eventType: "user action"
        eventClass: "optional"
        goto: 
        -   destination: "example_process_workflow"
            pathType: ["optional"]
        

pageTitle: "[]Page example 3 - in subsection]"


---

