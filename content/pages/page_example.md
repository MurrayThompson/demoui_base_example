---
title: "[Page Example]"
id: "page_example"

layout: page_example_template1

tags: [ "tag 1","tag 2"]
status: ["completed"]

pageGroup: 
    group: "example_group"
    version: "1.0"

workflows:
-   name: "example_workflow"
    onEvent: 
    -   event: " - action taken - "
        eventType: "user action"
        eventClass: "primary"
        goto: 
        -   destination: "page_example_2"
            pathType: ["primary"]
            when:
            -   case: "successful result" 
                conditions: 
                -   condition: "some condition"
                    type: "system"
                    state: "some value"
                -   condition: "some other-or-same condition"
                    type: "system"
                    state: "some other value"
            -   case: "another result" 
        -   destination: "page_example_3"
            pathType: ["secondary"]
            when:
            -   case: "something else occurs (no conditions)"
        -   destination: "no_page_yet_example"
            pathType: ["error"]
            when:
            -   case: "something else occurs"
                conditions: 
                -   condition: "some condition"
                    type: "system"
                    state: "some value"
        

pageTitle: "[Page example 1]"

showErrorMessage: false

---

{{<demoUI_only_annotations >}}

     {{< demoUI_only_annotation_item 
                title="[Some Note Title]" 
        >}}
            <div>[Note description text/html]</div>
        {{< /demoUI_only_annotation_item >}}
        

{{</demoUI_only_annotations >}}