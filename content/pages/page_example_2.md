---
title: "[Page Example 2]"
id: "page_example_2"

layout: page_example_template1

tags: [ "tag 1","tag 3"]
status: ["beta"]

pageGroup: 
    group: "example_group"
    version: "1.0"
    variant: "other version"

workflows:
-   name: "example_workflow"
    onEvent: 
    -   event: "some action taken"
        eventType: "user action"
        eventClass: "optional"
        goto: 
        -   destination: "page_example_3"
            pathType: ["optional"]

highlightedComponents:
    "component_example":
    "alert":
        

pageTitle: "[Page example 2]"

showSpecialMessage: true

---

{{<demoUI_only_annotations >}}

     {{< demoUI_only_annotation_item 
                title="[Some Note Title]" 
        >}}
            <div>[Note description text/html]</div>
        {{< /demoUI_only_annotation_item >}}
        

{{</demoUI_only_annotations >}}