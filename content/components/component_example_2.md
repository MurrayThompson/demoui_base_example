---
title: "[Component example 2]"
id: "component_example_2"

tags: [ "tag 1","tag 4"]

description: "[Summary description text of component]"

componentGroup: 
    group: "component_group_example"
    version: "2.0"

wrap_content: false


---

<div class="container-fluid">
    <h2>[some section title]</h2>
    <p>[some component information]</p>
</div>


<h2 class="demoUI_only demo_section_heading">[Demo example title]:</h2>

<div class="container-fluid">
    <p>[Explanation of component sample.]</p>
</div>

<section class="some_component_class">
    <div>
        <h2>[Component live example 2]</h2>
        <p>This is a live example of a component, just written out directly (not as a template or anything)</p>
    </div>
</section>


<h2 class="demoUI_only demo_section_heading">[Demo example title]:</h2>

<div class="container-fluid">
    <p>[Explanation of component sample.]</p>
</div>

<section class="some_component_class some_other_class">
    <div>
        <h2>[Component live example 2 - other state]</h2>
        <p>This is a live example of a component, just written out directly (not as a template or anything). But perhaps a bit different to highlight usage in a differnt context or state.</p>
    </div>
</section>