---
title: "[Component example]"
id: "component_example"

tags: [ "tag 1","tag 4"]
status: ["beta"]

description: "[Summary description text of component]"

componentGroup: 
    group: "component_group_example"
    version: "1.1"


---


<p>[this page can be any open layout, but generally will include h2 titles with related content and demo sections of fixed HTML examples for the component]</p>

<h2>[Some section title]</h2>
<ul>
<li>a list item 1</li>
<li>a list item 2</li>
<li>a list item 3</li>
</ul>


<h2>[Some section title]</h2>

<p>[some descriptive text about the component]</p>

<h2>[Some simple example of the component]</h2>
<p>[This component is not very special]</p>

